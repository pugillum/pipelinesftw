# PipelinesFTW

A demonstration of pipeline use including:
- lint and test on merge
- lint of Dockerfile and push to container registry on push to master
